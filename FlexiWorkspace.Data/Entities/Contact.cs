﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlexiWorkspace.Data.Entities;

/// <summary>
/// Entité représentant les données de contact d'un client
/// ou d'un lieu de travail.
/// </summary>
public partial class Contact : ObservableObject
{
    public int ContactId { get; set; }
    public string? Street { get; set; }
    public string? Number { get; set; }
    public int? Box { get; set; }
    public int? Zip { get; set; }
    public string? City { get; set; }
    public string FullAddress => $"{Street}, {Number} - {Zip} {City}";
    public string? Phone { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime? ModificationDate { get; set; }
    public virtual Actor? Actor { get; set; }
    public virtual Workplace? Workplace { get; set; }
}