﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlexiWorkspace.Data.Entities;

/// <summary>
/// Entité représentant un client de la société.
/// Peut être un particulier ou une entreprise.
/// </summary>
public partial class Actor : ObservableObject
{
    public int ActorId { get; set; }    
    public int ContactId { get; set; }
    public string? OrganisationName { get; set; }
    public string? LegalStatus { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Name => OrganisationName == null ? $"{LastName} {FirstName}" : $"{OrganisationName} {LegalStatus}";
    public string? Vat { get; set; }
    public string? Mobile { get; set; }
    public string? Email { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime? ModificationDate { get; set; }
    public bool Active { get; set; }
    public DateTime? InactiveDate { get; set; }
    public virtual Contact Contact { get; set; }
    public int ContractNum => Contracts.Count;
    public virtual ICollection<Contract> Contracts { get; set; } = new List<Contract>();
    public int WorkplaceNum => Workplaces.Count;
    public virtual ICollection<Workplace> Workplaces { get; set; } = new List<Workplace>();

    public Actor()
    {
        Contact = new() { CreationDate = DateTime.Now };
    }
}