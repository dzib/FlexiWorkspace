﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlexiWorkspace.Data.Entities;

/// <summary>
/// Entité représentant un espace de travail.
/// </summary>
public partial class Workplace : ObservableObject
{
    public int WorkplaceId { get; set; }
    public int? ContactId { get; set; }
    public int? RenterId { get; set; }
    public int? Rent { get; set; }
    public int? Surface { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime? ModificationDate { get; set; }
    public bool Active { get; set; }
    public DateTime? InactiveDate { get; set; }
    public EStatus? Status => Active && !Contracts.Any(c => (int)c.Status!.Value < 3) ? EStatus.Free : EStatus.Rented;
    public virtual Contact Contact { get; set; }
    public virtual ICollection<Contract> Contracts { get; set; } = new List<Contract>();
    public virtual Actor? Renter { get; set; }
    public virtual ICollection<WorkplaceFeature> WorkplaceFeatures { get; set; } = new List<WorkplaceFeature>();

    public enum EStatus
    {
        Unknown = 0,
        Free = 1,
        Rented = 2
    }

    public Workplace()
    {
        Contact = new() { CreationDate = DateTime.Now };
    }
}