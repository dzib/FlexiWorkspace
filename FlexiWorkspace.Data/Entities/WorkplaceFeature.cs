﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;

namespace FlexiWorkspace.Data.Entities;

/// <summary>
/// Entité représentant un équipement ou une caractéristique présente
/// dans un espace de travail.
/// </summary>
public partial class WorkplaceFeature : ObservableObject
{
    public int WorkplaceFeatureId { get; set; }
    public int WorkplaceId { get; set; }
    public int FeatureId { get; set; }
    public bool Active { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime? RemovalDate { get; set; }
    public virtual Feature? Feature { get; set; }
    public virtual Workplace? Workplace { get; set; }
}