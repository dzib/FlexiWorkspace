﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.ComponentModel;

namespace FlexiWorkspace.Data.Entities;

/// <summary>
/// Entité représentant un contrat.
/// </summary>
public partial class Contract : ObservableObject
{
    [ObservableProperty] public int? _rent;

    public int ContractId { get; set; }
    public int WorkplaceId { get; set; }
    public int? TenantId { get; set; }    
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public int? Discount { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime? ModificationDate { get; set; }
    public bool Cancelled { get; set; }

    public DateTime? CancelledDate { get; set; }
    public EStatus? Status => Cancelled ? EStatus.Cancelled
        : DateTime.Now < StartDate ? EStatus.Planned
        : DateTime.Now > EndDate ? EStatus.Archived
        : EStatus.Running;
    public virtual Actor? Tenant { get; set; }
    public virtual Workplace? Workplace { get; set; }
    public enum EStatus
    {
        [Description("Tous")]
        All = 0,
        [Description("Indéfini")]
        Undefined = 1,
        [Description("Planifié")]
        Planned = 2,
        [Description("En cours")]
        Running = 3,
        [Description("Archivé")]
        Archived = 4,
        [Description("Annulé")]
        Cancelled = 5
    }

    public Contract() { }
    public Contract(Contract contract)
    {
        WorkplaceId = contract.WorkplaceId;
        Workplace = contract.Workplace;
        CreationDate = contract.CreationDate;
        StartDate = contract.StartDate;
        EndDate = contract.EndDate;
        Discount = 0;
        Cancelled = false;
    }
}