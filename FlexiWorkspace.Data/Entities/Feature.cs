﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.Generic;

namespace FlexiWorkspace.Data.Entities;

/// <summary>
/// Entité représentant un équipement ou une caractéristique
/// pouvant être attribué à un lieu de travail.
/// </summary>
public partial class Feature : ObservableObject
{
    public int FeatureId { get; set; }
    public string? Label { get; set; }
    public virtual ICollection<WorkplaceFeature> WorkplaceFeatures { get; set; } = new List<WorkplaceFeature>();
}