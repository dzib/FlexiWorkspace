﻿using FlexiWorkspace.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FlexiWorkspace.Data.Repositories;

public partial class GenericRepository : IGenericRepository
{
    private readonly FlexiDbContext Context;

    public GenericRepository(FlexiDbContext context)
    {
        Context = context;
    }

    /// <summary>
    /// Ajoute une entité spécifique dans le contexte.
    /// </summary>
    /// <typeparam name="TEntity">Type de l'entité à ajouter.</typeparam>
    /// <param name="entity">Entité à ajouter.</param>
    public void Add<TEntity>(TEntity entity) where TEntity : class
    {
        Context.Set<TEntity>().Add(entity);        
    }

    /// <summary>
    /// Ajoute une plage d'entités dans le contexte.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité à ajouter.</typeparam>
    /// <param name="entities">Ensemble d'entités à ajouter.</param>
    public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
    {
        Context.Set<TEntity>().AddRange(entities);
    }

    /// <summary>
    /// Supprime une entité par ID.
    /// </summary>
    /// <typeparam name="TEntity">Type de l'entité à supprimer.</typeparam>
    /// <param name="id">ID de l'entité à supprimer.</param>
    public void Delete<TEntity>(object id) where TEntity : class
    {
        var entity = Context.Set<TEntity>().Find(id);
        if (entity != null) 
            Delete(entity);
    }

    /// <summary>
    /// Supprime une entité spécifique.
    /// </summary>
    /// <typeparam name="TEntity">Type de l'entité à supprimer.</typeparam>
    /// <param name="entity">Entité à supprimer.</param>
    public void Delete<TEntity>(TEntity entity) where TEntity : class
    {
        var dbSet = Context.Set<TEntity>();
        if (Context.Entry(entity).State == EntityState.Detached)
            dbSet.Attach(entity);
        dbSet.Remove(entity);
    }

    /// <summary>
    /// Exécute une requête SQL asynchrone.
    /// </summary>
    /// <param name="sql">Requête SQL à exécuter.</param>
    /// <param name="parameters">Paramètres de la requête SQL.</param>
    /// <returns>Entier représentant le nombre de lignes affectées.</returns>
    public async virtual Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters)
    {
        return await Context.Database.ExecuteSqlRawAsync(sql, parameters);
    }

    /// <summary>
    /// Récupère une liste d'entités en fonction de différents critères.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité à récupérer.</typeparam>
    /// <param name="filter">Filtre pour la récupération des entités.</param>
    /// <param name="orderBy">Critère de tri des entités.</param>
    /// <param name="includeProperties">Propriétés à inclure dans la récupération.</param>
    /// <param name="pageNumber">Numéro de la page.</param>
    /// <param name="pageSize">Taille de la page.</param>
    /// <returns>Liste des entités récupérées.</returns>
    public virtual IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null, string? includeProperties = null, int pageNumber = -1, int pageSize = -1) where TEntity : class
    {
        return GetQueryable(filter, orderBy, includeProperties, pageNumber, pageSize).ToList();
    }

    /// <summary>
    /// Récupère toutes les entités d'un type spécifique avec des options de pagination et de filtrage.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité à récupérer.</typeparam>
    /// <param name="orderBy">Critère de tri des entités.</param>
    /// <param name="includeProperties">Propriétés à inclure dans la récupération.</param>
    /// <param name="pageNumber">Numéro de la page.</param>
    /// <param name="pageSize">Taille de la page.</param>
    /// <returns>Liste des entités récupérées après application des options de pagination et de tri.</returns>
    public virtual IEnumerable<TEntity> GetAll<TEntity>(string? orderBy = null, string? includeProperties = null, int pageNumber = -1, int pageSize = -1) where TEntity : class
    {
        return GetQueryable<TEntity>(null, orderBy, includeProperties, pageNumber, pageSize).ToList();
    }

    /// <summary>
    /// Récupère une entité spécifique en fonction de son ID.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité à récupérer.</typeparam>
    /// <param name="id">ID de l'entité à récupérer.</param>
    /// <returns>L'entité correspondant à l'ID spécifié, ou null si elle n'est pas trouvée.</returns>
    public virtual TEntity? GetById<TEntity>(object id) where TEntity : class
    {
        return Context.Set<TEntity>().Find(id);
    }

    /// <summary>
    /// Obtient le nombre total d'entités d'un type spécifique en fonction d'un filtre donné.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité pour le comptage.</typeparam>
    /// <param name="filter">Filtre pour le comptage des entités.</param>
    /// <returns>Nombre total d'entités selon le filtre donné.</returns>
    public virtual int GetCount<TEntity>(Expression<Func<TEntity, bool>>? filter = null) where TEntity : class
    {
        return GetQueryable(filter).Count();
    }

    /// <summary>
    /// Vérifie si au moins une entité du type spécifié existe en fonction d'un filtre donné.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité à vérifier.</typeparam>
    /// <param name="filter">Filtre pour la vérification de l'existence d'entités.</param>
    /// <param name="orderBy">Critère de tri pour la vérification d'existence.</param>
    /// <returns>True si au moins une entité correspondant au filtre donné existe, sinon False.</returns>
    public bool GetExists<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null) where TEntity : class
    {
        return GetQueryable(filter, orderBy).Any();
    }

    /// <summary>
    /// Récupère la première entité correspondant à un filtre donné avec des options de tri et d'inclusion de propriétés.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité à récupérer.</typeparam>
    /// <param name="filter">Filtre pour récupérer la première entité.</param>
    /// <param name="orderBy">Critère de tri des entités.</param>
    /// <param name="includeProperties">Propriétés à inclure dans la récupération.</param>
    /// <returns>La première entité correspondant au filtre donné ou null si aucune n'est trouvée.</returns>
    public virtual TEntity? GetFirst<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null, string? includeProperties = null) where TEntity : class
    {
        return GetQueryable(filter, orderBy, includeProperties).FirstOrDefault();
    }

    /// <summary>
    /// Enregistre tous les changements effectués dans le contexte.
    /// </summary>
    public void Save()
    {
        Context.SaveChanges();
    }

    /// <summary>
    /// Calcule la somme d'une propriété spécifique d'une entité en fonction d'un filtre donné.
    /// </summary>
    /// <typeparam name="TEntity">Type de l'entité pour le calcul.</typeparam>
    /// <param name="selector">Expression indiquant la propriété de l'entité à sommer.</param>
    /// <param name="filter">Filtre pour les entités à considérer dans le calcul de la somme.</param>
    /// <returns>La somme des valeurs de la propriété spécifiée pour les entités filtrées.</returns>
    public virtual decimal? Sum<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>>? filter = null) where TEntity : class
    {
        return GetQueryable(filter).Sum(selector);
    }

    /// <summary>
    /// Met à jour une entité spécifique.
    /// </summary>
    /// <typeparam name="TEntity">Type de l'entité à mettre à jour.</typeparam>
    /// <param name="entity">Entité à mettre à jour.</param>
    public void Update<TEntity>(TEntity entity) where TEntity : class
    {
        Context.Set<TEntity>().Attach(entity);
        Context.Entry(entity).State = EntityState.Modified;
    }

    /// <summary>
    /// Obtient une requête IQueryable pour une entité spécifique en fonction de divers critères.
    /// </summary>
    /// <typeparam name="TEntity">Type d'entité pour la requête.</typeparam>
    /// <param name="filter">Filtre pour la requête.</param>
    /// <param name="orderBy">Critère de tri pour la requête.</param>
    /// <param name="includeProperties">Propriétés à inclure dans la requête.</param>
    /// <param name="pageNumber">Numéro de la page pour la pagination.</param>
    /// <param name="pageSize">Taille de la page pour la pagination.</param>
    /// <param name="sourceQuery">Query source pour la construction.</param>
    /// <returns>Requête IQueryable basée sur les critères fournis.</returns>
    protected virtual IQueryable<TEntity> GetQueryable<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null, string? includeProperties = null, int pageNumber = -1, int pageSize = -1, IQueryable<TEntity>? sourceQuery = null) where TEntity : class
    {
        includeProperties ??= string.Empty;
        IQueryable<TEntity> query = sourceQuery ?? Context.Set<TEntity>();

        if (filter != null)
            query = query.Where(filter);

        query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty.Trim()));

        if (orderBy != null)
            query = query.OrderBy(orderBy);

        if (pageNumber != -1)
            query = query.Skip((pageNumber - 1) * pageSize);

        if (pageSize != -1)
            query = query.Take(pageSize);

        return query;
    }
}
