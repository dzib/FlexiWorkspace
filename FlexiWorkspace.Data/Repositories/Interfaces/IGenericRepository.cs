﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FlexiWorkspace.Data.Repositories.Interfaces;

public interface IGenericRepository
{
    void Add<TEntity>(TEntity entity) where TEntity : class;
    void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
    void Delete<TEntity>(object id) where TEntity : class;
    void Delete<TEntity>(TEntity entity) where TEntity : class;
    Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters);
    IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null, string? includeProperties = null, int pageNumber = -1, int pageSize = -1) where TEntity : class;
    IEnumerable<TEntity> GetAll<TEntity>(string? orderBy = null, string? includeProperties = null, int pageNumber = -1, int pageSize = -1) where TEntity : class;
    TEntity? GetById<TEntity>(object id) where TEntity : class;
    int GetCount<TEntity>(Expression<Func<TEntity, bool>>? filter = null) where TEntity : class;
    bool GetExists<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null) where TEntity : class;
    TEntity? GetFirst<TEntity>(Expression<Func<TEntity, bool>>? filter = null, string? orderBy = null, string? includeProperties = null) where TEntity : class;
    void Save();
    decimal? Sum<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>>? filter = null) where TEntity : class;
    void Update<TEntity>(TEntity entity) where TEntity : class;
}