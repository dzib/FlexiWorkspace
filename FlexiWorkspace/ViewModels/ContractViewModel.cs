﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using log4net;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows;
using static FlexiWorkspace.Data.Entities.Contract;

namespace FlexiWorkspace.ViewModels;

public partial class ContractViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
    readonly IContractService contractService;
    readonly ContractsViewModel contractsViewModel;
    readonly bool isNew;

    [ObservableProperty] Contract _contract;
    [ObservableProperty] Workplace? _workplace;
    [ObservableProperty] IEnumerable<Workplace>? _workplaces;
    [ObservableProperty] IEnumerable<Actor>? _actors;
    [ObservableProperty] EStatus? _status;
    [ObservableProperty] string? _strStartDate;
    [ObservableProperty] string? _strEndDate;
    [ObservableProperty] string? _message;
    [ObservableProperty] bool _messageVisible;
    [ObservableProperty] bool _create = false;
    [ObservableProperty] bool _view = false;
    [ObservableProperty] string _title;

    public ContractViewModel(IActorService actorService, IContractService contractService, IWorkplaceService workplaceService, ContractsViewModel contractsViewModel, Contract contract, bool isNew)
    {
        this.contractService = contractService;
        this.contractsViewModel = contractsViewModel;
        this.isNew = isNew;

        Contract = contract;
        if (isNew)
        {
            Status = EStatus.Planned;
            Actors = actorService.GetAllActors().OrderBy(a => a.Name);
            Workplaces = workplaceService.GetAllWorkplaces().OrderBy(w => w.Contact?.FullAddress);
            Contract.CreationDate = DateTime.Now;
            Contract.StartDate = DateTime.Now.AddDays(1);
            Contract.EndDate = DateTime.Now.AddDays(1).AddYears(1);
            Contract.Discount = 0;
            Create = true;
            Title = "Nouveau contrat";
        }
        else
        {
            Status = EStatus.Planned;
            View = true;
            Title = $"Contrat numéro {Contract.ContractId}";
        }
        StrStartDate = Contract?.StartDate > DateTime.Now ? "Démarre le" : "Démarré le";
        StrEndDate = Contract?.EndDate > DateTime.Now ? "Se termine le" : "S'est terminé le";
        MessageVisible = false;

        log.Info("ContractViewModel created");
    }

    [RelayCommand]
    void UpdateWorkplace()
    {
        Workplace = Contract.Workplace;
        UpdateDiscount();
    }

    [RelayCommand]
    void UpdateStartDate()
    {
        Status = Contract.Status;
    }

    [RelayCommand]
    void UpdateEndDate(string date)
    {
        if (Contract.EndDate < Contract.StartDate)
            Notify("La date de fin ne peut être inférieure ou égale à la date de début");
        else if (Contract.EndDate <= Contract.StartDate.AddMonths(1))
            Notify("Un contrat ne peut durer moins d'un mois");
        else MessageVisible = false;
        Status = Contract.Status;
    }

    [RelayCommand]
    void UpdateDiscount() 
    {
        Contract!.Rent = Contract.Workplace?.Rent - (Contract.Workplace?.Rent / 100 * Contract.Discount);
    }

    [RelayCommand]
    void SaveContract()
    {
        if (isNew)
        {
            if (Contract.Tenant == null)
                Notify("Veuillez d'abord attribuer un locataire");
            else if (Contract.StartDate < DateTime.Now)
                Notify("Veuillez d'abord attribuer une date de début de contrat valide");
            else if (Contract.EndDate < Contract.StartDate)
                Notify("Veuillez d'abord attribuer une date de fin de contrat valide");
            else if (contractService.CheckOverlappingDates(Contract))
                Notify("Il existe déjà un contrat pour ce bureau aux dates sélectionnées");
            else
            {
                contractService.AddContract(Contract);
                contractsViewModel.FilterContracts();
                Notify("Contrat créé");
                Contract = new(Contract);
            }
        }
        else
        {
            contractService.UpdateContract(Contract!);
            Notify("Contrat sauvegardé");
        }
    }

    [RelayCommand]
    void CancelContract()
    {
        contractService.CancelContract(Contract!);
        Notify("Contrat annulé");
    }

    [RelayCommand]
    static void Close(Window window)
    {
        window.Close();
    }

    void Notify(string message)
    {
        Message = message;
        MessageVisible = true;
        Timer timer = new(5000);
        timer.Elapsed += TimerElapsed!;
        timer.AutoReset = false;
        timer.Start();
    }

    void TimerElapsed(object sender, ElapsedEventArgs e) => MessageVisible = false;
}
