﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Common.Entities;
using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Views;
using log4net;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;

namespace FlexiWorkspace.ViewModels;

public partial class ContractsViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
    readonly IFactory<ContractViewModel> factory;
    readonly IContractService contractService;

    [ObservableProperty] Contract? _selectedContract;
    [ObservableProperty] PagedResult<Contract>? _contracts;
    [ObservableProperty] ContractFilter _filter;
    [ObservableProperty] IEnumerable<int> _resultsNumber;

    public ContractsViewModel(IFactory<ContractViewModel> factory, IContractService contractService)
    {
        this.factory = factory;
        this.contractService = contractService;

        ResultsNumber = new List<int>() { 10, 25, 50, 100 };
        Filter = new();
        FilterContracts();

        log.Info("ContractsViewModel created");
    }

    [RelayCommand]
    public void FilterContracts(string? reset = null)
    {
        if (reset == "1")
            Filter.PageNumber = 1;
        (IEnumerable<Contract> contracts, int count) = contractService.GetContractsByFilter(Filter);
        Contracts = new(contracts, Filter.PageNumber, Filter.PageSize, count);
    }

    [RelayCommand]
    void ViewContract()
    {
        if (SelectedContract != null)
        {
            ContractView contractView = new() { DataContext = factory.CreateViewModel(SelectedContract) };
            contractView.Show();
            SelectedContract = null;
        }
    }

    [RelayCommand]
    void CreateContract()
    {
        ContractView contractView = new() { DataContext = factory.CreateViewModel(new Contract(), true) };
        contractView.Show();
    }

    [RelayCommand]
    void FirstPage()
    {
        if (Filter.PageNumber > 1)
        {
            Filter.PageNumber = 1;
            FilterContracts();
        }
    }        

    [RelayCommand]
    void PreviousPage()
    {
        if (Filter.PageNumber > 1)
        {
            Filter.PageNumber--;
            FilterContracts();
        }
    }

    [RelayCommand]
    void NextPage()
    {
        if (Filter.PageNumber < Contracts?.TotalPages)
        {
            Filter.PageNumber++;
            FilterContracts();
        }
    }

    [RelayCommand]
    void LastPage()
    {
        if (Filter.PageNumber < Contracts?.TotalPages)
        {
            Filter.PageNumber = Contracts.TotalPages;
            FilterContracts();
        }
    }

    [RelayCommand]
    static void Close(Window window)
    {
        window.Visibility = Visibility.Collapsed;
    }
}
