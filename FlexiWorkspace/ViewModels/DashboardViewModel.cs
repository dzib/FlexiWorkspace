﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Entities;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Views;
using log4net;
using OxyPlot.Series;
using OxyPlot;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Contract = FlexiWorkspace.Data.Entities.Contract;
using System.Collections.Generic;
using OxyPlot.Axes;
using System.Windows.Media;
using System.Globalization;
using Newtonsoft.Json.Linq;
using OxyPlot.Annotations;
using System.Collections.ObjectModel;
using System.ComponentModel;
using FlexiWorkspace.Themes;
using System.Windows.Controls;

namespace FlexiWorkspace.ViewModels;

public partial class DashboardViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);

    readonly IDashboardService dashboardService;
    readonly ActorsView actorsView;
    readonly ContractsView contractsView;
    readonly WorkplacesView workplacesView;
    readonly IFactory<ActorViewModel> actorFactory;
    readonly IFactory<ContractViewModel> contractFactory;
    readonly Dashboard dashboard;

    readonly DispatcherTimer _timer;
    [ObservableProperty] DateTime _currentDate;
    [ObservableProperty] Actor? _selectedActor;
    [ObservableProperty] Contract? _selectedContract;
    [ObservableProperty] PlotModel? _pieChart;
    [ObservableProperty] PlotModel? _barChart;
    [ObservableProperty] double _percentRented;
    [ObservableProperty] double _percentUnrented;
    [ObservableProperty] string? _strActors;
    [ObservableProperty] string? _strOrganisations;
    [ObservableProperty] string? _strPersons;
    [ObservableProperty] string? _strWorkplaces;
    [ObservableProperty] string? _strWorkplacesAvailable;
    [ObservableProperty] string? _strRunningContracts;
    [ObservableProperty] string? _strSumRents;
    [ObservableProperty] string? _strAverageDuration;
    [ObservableProperty] EBarChartSource _barChartSource;
    [ObservableProperty] List<int> _months;
    [ObservableProperty] int _numberOfMonths;
    [ObservableProperty] ObservableCollection<Actor> _actors;
    [ObservableProperty] ObservableCollection<Contract> _contracts;
    [ObservableProperty] string? _theme;

    public enum EBarChartSource
    {
        [Description("Clients")]
        Actors = 0,
        [Description("Contrats")]
        Contracts = 1,
        [Description("Lieux de travail")]
        Workplaces = 2
    }

    public DashboardViewModel(IDashboardService dashboardService,
        ActorsView actorsView, ContractsView contractsView, WorkplacesView workplacesView,
        IFactory<ActorViewModel> actorFactory, IFactory<ContractViewModel> contractFactory)
    {
        this.dashboardService = dashboardService;
        this.actorsView = actorsView;
        this.contractsView = contractsView;
        this.workplacesView = workplacesView;
        this.actorFactory = actorFactory;
        this.contractFactory = contractFactory;

        dashboard = new();
        BarChartSource = EBarChartSource.Contracts;
        Months = [3, 6, 12, 24];
        Actors = new(dashboardService.GetLastActors(5));
        Contracts = new(dashboardService.GetEndingContracts(5));
        Theme = $"Theme {Settings.Default.Theme}";

        _timer = new(DispatcherPriority.Render) { Interval = TimeSpan.FromSeconds(1) };
        _timer.Tick += (sender, args) => CurrentDate = DateTime.Now;
        _timer.Start();

        DashboardInitialization();
        PieChartInitialization();
        BarChartInitialization();

        log.Info("DashboardViewModel created");
    }

    void DashboardInitialization()
    {
        dashboard.Actors = dashboardService.GetActorsCount();
        dashboard.Organisations = dashboardService.GetOrganisationsCount();
        dashboard.Persons = dashboardService.GetPersonsCount();
        dashboard.Workplaces = dashboardService.GetWorkplacesCount();
        dashboard.WorkplacesAvailable = dashboardService.GetWorkplacesAvailableCount();
        dashboard.RunningContracts = dashboardService.GetRunningContractsCount();
        dashboard.SumRents = dashboardService.GetSumRents();
        dashboard.AverageDuration = dashboardService.GetAverageLength();

        PercentRented = ((double)(dashboard.Workplaces - dashboard.WorkplacesAvailable) / dashboard.Workplaces) * 100;
        PercentUnrented = ((double)dashboard.WorkplacesAvailable / dashboard.Workplaces) * 100;


        #region TextFormatting
        string s;

        s = dashboard.Actors > 1 ? "s" : string.Empty;
        StrActors = $"{dashboard.Actors} client{s}";

        s = dashboard.Organisations > 1 ? "s" : string.Empty;
        StrOrganisations = $"({dashboard.Organisations} société{s} et";

        s = dashboard.Persons > 1 ? "s" : string.Empty;
        StrPersons = $"{dashboard.Persons} particulier{s})";

        s = dashboard.Workplaces > 1 ? "s" : string.Empty;
        StrWorkplaces = $"{dashboard.Workplaces} lieux de travail dont";

        s = dashboard.WorkplacesAvailable > 1 ? "s" : string.Empty;
        StrWorkplacesAvailable = $"{dashboard.WorkplacesAvailable} disponible{s}";

        s = dashboard.RunningContracts > 1 ? "s" : string.Empty;
        StrRunningContracts = $"{dashboard.RunningContracts} contrat{s} en cours";

        StrSumRents = $"{dashboard.SumRents:C} générés chaque mois par les loyers";

        TimeSpan length = dashboard.AverageDuration;
        int years = (int)(length.Days / 365.25);
        int months = (int)(length.Days % 365.25 / 30.44);
        s = years > 1 ? "s" : string.Empty;
        StrAverageDuration = $"La durée moyenne d'un contrat est de {years} an{s} et {months} mois";
        #endregion
    }

    [RelayCommand]
    void ChangeSource()
    {
        BarChartInitialization(NumberOfMonths, BarChartSource);
    }

    void PieChartInitialization()
    {
        PieChart = new();
        PieChart.MouseDown += (sender, e) => e.Handled = true;
        PieSeries serie = new() { TextColor = OxyColors.Transparent };        

        serie.Slices.Add(new PieSlice("", dashboard.Workplaces - dashboard.WorkplacesAvailable) 
        { 
            Fill = OxyColor.Parse("#527853") 
        });
        serie.Slices.Add(new PieSlice("", dashboard.WorkplacesAvailable) 
        { 
            Fill = OxyColor.Parse("#9A3B3B") 
        });

        PieChart.Series.Add(serie);
    }

    void BarChartInitialization(int numberOfMonths = 12, EBarChartSource? source = null)
    {
        var months = new List<string>();
        var values = new List<int>();
        source ??= EBarChartSource.Contracts;

        DateTime date = DateTime.Now.AddMonths(-numberOfMonths + 1);
        int month = date.Month;
        int year = date.Year;

        for (int i = 0; i < numberOfMonths; i++)
        {
            string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month);
            if (numberOfMonths > 12)
                months.Add($"{month}/{year.ToString()[2..]}");
            else
                months.Add($"{monthName} {year.ToString()[2..]}");
            if (source == EBarChartSource.Actors) 
                values.Add(dashboardService.GetActorsCountByMonth(year, month));
            else if (source == EBarChartSource.Contracts)
                values.Add(dashboardService.GetContractsCountByMonth(year, month));
            else if (source == EBarChartSource.Workplaces)
                values.Add(dashboardService.GetWorkplacesCountByMonth(year, month));

            month++;
            if (month == 13)
            {
                month = 1;
                year++;
            }
        }

        Color fontColor = (Color)Application.Current.Resources["FontColor"];
        Color barColor = (Color)Application.Current.Resources["AccentColor"];
        Color valueColor = (Color)Application.Current.Resources["SecondaryColor"];

        BarChart = new()
        {
            TextColor = OxyColor.FromArgb(fontColor.A, fontColor.R, fontColor.G, fontColor.B),
            PlotAreaBorderColor = OxyColor.FromArgb(barColor.A, barColor.R, barColor.G, barColor.B), 
            PlotAreaBorderThickness = new OxyThickness(0, 0, 0, 3),
            DefaultFont = "Bahnschrift"
        };
        if (numberOfMonths > 12)
            BarChart.DefaultFontSize = 10;
        else if (numberOfMonths < 6)
            BarChart.DefaultFontSize = 18;
        else if (numberOfMonths < 12)
            BarChart.DefaultFontSize = 16;
        else BarChart.DefaultFontSize = 12;

        BarChart.MouseDown += (sender, e) => e.Handled = true;

        CategoryAxis xAxis = new()
        { 
            TickStyle = TickStyle.None, 
            IsPanEnabled = false, 
            IsZoomEnabled = false
        };

        LinearAxis yAxis = new()
        {
            TickStyle = TickStyle.None,
            TextColor = OxyColors.Transparent,
            IsPanEnabled = false,
            IsZoomEnabled = false
        };

        LinearBarSeries barSeries = new() 
        {  
            FillColor = OxyColor.FromArgb(barColor.A, barColor.R, barColor.G, barColor.B)
        };
        if (numberOfMonths > 12)
            barSeries.BarWidth = 20;
        else if (numberOfMonths < 6)
            barSeries.BarWidth = 100;
        else if (numberOfMonths < 12)
            barSeries.BarWidth = 60;
        else barSeries.BarWidth = 30;

        for (int i = 0; i < months.Count; i++)
        {
            barSeries.Points.Add(new DataPoint(i, values[i]));
            var barAnnotation = new RectangleAnnotation
            {
                Text = values[i].ToString(),
                TextColor = OxyColor.FromArgb(valueColor.A, valueColor.R, valueColor.G, valueColor.B),
                FontSize = 16,
                Font = "Bahnschrift",
                TextPosition = new DataPoint(i, values[i] - 0.6),
                TextHorizontalAlignment = OxyPlot.HorizontalAlignment.Center,
                TextVerticalAlignment = OxyPlot.VerticalAlignment.Middle,
                Fill = OxyColors.Transparent
            };
            BarChart.Annotations.Add(barAnnotation);
            xAxis.Labels.Add(months[i].ToString());
        }

        BarChart.Series.Add(barSeries);
        BarChart.Axes.Add(xAxis);
        BarChart.Axes.Add(yAxis);
        BarChart.InvalidatePlot(true);
    }

    [RelayCommand]
    void ViewActors()
    {
        actorsView.Show();
    }

    [RelayCommand]
    void ViewContracts()
    {
        contractsView.Show();
    }

    [RelayCommand]
    void ViewWorkplaces()
    {
        workplacesView.Show();
    }

    [RelayCommand]
    void ViewActor()
    {
        if (SelectedActor != null)
        {
            ActorView actorView = new() { DataContext = actorFactory.CreateViewModel(SelectedActor) };
            actorView.Show();
            SelectedActor = null;
        }
    }

    [RelayCommand]
    void ViewContract()
    {
        if (SelectedContract != null)
        {
            ContractView contractView = new() { DataContext = contractFactory.CreateViewModel(SelectedContract) };
            contractView.Show();
            SelectedContract = null;
        }
    }

    [RelayCommand]
    void ThemeSelect(string header)
    {
        ThemeManager.LoadTheme(header);
        Theme = $"Theme {header}";
        Settings.Default.Theme = header;
        BarChartInitialization();
    }

    [RelayCommand]
    static void Close()
    {
        Settings.Default.Save();
        Application.Current.Shutdown();
    }
}
