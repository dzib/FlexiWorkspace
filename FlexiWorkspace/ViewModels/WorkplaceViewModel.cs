﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using log4net;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows;

namespace FlexiWorkspace.ViewModels;

public partial class WorkplaceViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
    readonly IWorkplaceService workplaceService;

    [ObservableProperty] Workplace _workplace;
    [ObservableProperty] IEnumerable<Contract>? _contracts;
    [ObservableProperty] Contract _selectedContract;
    [ObservableProperty] Feature _selectedFeature;
    [ObservableProperty] IEnumerable<Feature>? _features;
    [ObservableProperty] WorkplaceFeature _selectedWorkplaceFeature;
    [ObservableProperty] ObservableCollection<WorkplaceFeature> _workplaceFeatures;
    [ObservableProperty] IEnumerable<Actor>? _actors;
    [ObservableProperty] string? _message;
    [ObservableProperty] bool _messageVisible;

    public WorkplaceViewModel(IActorService actorService, IContractService contractService, IWorkplaceService workplaceService, Workplace workplace, bool isNew)
    {
        this.workplaceService = workplaceService;

        SelectedFeature = new();
        SelectedContract = new();
        SelectedWorkplaceFeature = new();
        Workplace = workplace;
        Actors = actorService.GetAllActors().OrderBy(a => a.Name);
        Features = workplaceService.GetAllFeatures();
        Contracts = contractService.GetContracts(c => c.WorkplaceId == Workplace.WorkplaceId);
        WorkplaceFeatures = new(Workplace.WorkplaceFeatures);
        MessageVisible = false;

        log.Info("WorkplaceViewModel created");
    }

    [RelayCommand]
    void AddWorkplaceFeature()
    {
        WorkplaceFeature workplaceFeature = new()
        {
            WorkplaceId = Workplace.WorkplaceId,
            Workplace = Workplace,
            FeatureId = SelectedFeature.FeatureId,
            Feature = SelectedFeature
        };

        WorkplaceFeatures.Add(workplaceFeature);
        workplaceService.AddWorkplaceFeature(workplaceFeature);
    }

    [RelayCommand]
    void RemoveWorkplaceFeature()
    {
        workplaceService.RemoveWorkplaceFeature(SelectedWorkplaceFeature);
        WorkplaceFeatures.Remove(SelectedWorkplaceFeature);
    }

    [RelayCommand]
    void ViewContract()
    {
        //ContractView view = new();
        //view.Show();
    }

    [RelayCommand]
    void SaveWorkplace()
    {
        workplaceService.UpdateWorkplace(Workplace);
        Notify("Bureau sauvegardé");
    }

    [RelayCommand]
    void DeactivateWorkplace()
    {
        workplaceService.DeactivateWorkplace(Workplace);
        Notify("Bureau désactivé");
    }

    [RelayCommand]
    public static void Close(Window window)
    {
        window.Close();
    }

    void Notify(string message)
    {
        Message = message;
        MessageVisible = true;
        Timer timer = new(5000);
        timer.Elapsed += TimerElapsed!;
        timer.AutoReset = false;
        timer.Start();
    }

    void TimerElapsed(object sender, ElapsedEventArgs e) => MessageVisible = false;
}
