﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using log4net;
using System;
using System.Reflection;
using System.Timers;
using System.Windows;

namespace FlexiWorkspace.ViewModels;

public partial class ActorViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
    readonly IActorService actorService;
    readonly bool isNew;

    [ObservableProperty] Actor _actor;
    [ObservableProperty] string? _strName1;
    [ObservableProperty] string? _strName2;
    [ObservableProperty] string? _name1;
    [ObservableProperty] string? _name2;
    [ObservableProperty] bool _organisation;
    [ObservableProperty] bool _create = false;
    [ObservableProperty] bool _view = false;
    [ObservableProperty] string _title;
    [ObservableProperty] string? _message;
    [ObservableProperty] bool _messageVisible;

    public ActorViewModel(IActorService actorService, Actor actor, bool isNew)
    {
        this.actorService = actorService;
        this.isNew = isNew;

        Actor = actor;          
        
        if (isNew)
        {
            Actor.OrganisationName = string.Empty;
            Actor.CreationDate = DateTime.Now;
            Create = true;
            Title = "Nouveau client";
        }
        else
        {
            View = true;
            Title = actor.Name!;
        }

        Organisation = Actor.OrganisationName != null;
        if (Organisation)
        {
            StrName1 = "Nom";
            StrName2 = "Statut";
            Name1 = Actor.OrganisationName;
            Name2 = Actor.LegalStatus;
        }
        else
        {
            StrName1 = "Nom";
            StrName2 = "Prénom";
            Name1 = Actor.LastName;
            Name2 = Actor.FirstName;
        }
        MessageVisible = false;

        log.Info("ActorViewModel created");
    }

    [RelayCommand]
    void UpdateName1()
    {
        if (Organisation)
            Actor.OrganisationName = Name1;
        else Actor.LastName = Name1;
    }

    [RelayCommand]
    void UpdateName2()
    {
        if (Organisation)
            Actor.LegalStatus = Name2;
        else Actor.FirstName = Name2;
    }    

    [RelayCommand]
    void SaveActor()
    {
        if (isNew)
        {
            actorService.AddActor(Actor);
            Message = "Client créé";
        }
        else
        {
            actorService.UpdateActor(Actor);
            Message = "Client sauvegardé";
        }
        MessageVisible = true;
        StartTimer();
    }

    [RelayCommand]
    void DeactivateActor()
    {
        actorService.DeactivateActor(Actor);
        Message = "client désactivé";
        MessageVisible = true;
        StartTimer();
    }

    [RelayCommand]
    public static void Close(Window window)
    {
        window.Close();
    }

    void StartTimer()
    {
        Timer timer = new(5000);
        timer.Elapsed += TimerElapsed!;
        timer.AutoReset = false;
        timer.Start();
    }

    void TimerElapsed(object sender, ElapsedEventArgs e) => MessageVisible = false;
}
