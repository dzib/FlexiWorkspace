﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Common.Entities;
using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Views;
using log4net;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;

namespace FlexiWorkspace.ViewModels;

public partial class WorkplacesViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
    readonly IFactory<WorkplaceViewModel> factory;
    readonly IWorkplaceService workplaceService;

    [ObservableProperty] Workplace? _selectedWorkplace;
    [ObservableProperty] PagedResult<Workplace>? _workplaces;
    [ObservableProperty] WorkplaceFilter _filter;
    [ObservableProperty] IEnumerable<int> _resultsNumber;

    public WorkplacesViewModel(IFactory<WorkplaceViewModel> factory, IWorkplaceService workplaceService)
    {
        this.factory = factory;
        this.workplaceService = workplaceService;

        ResultsNumber = new List<int>() { 10, 25, 50, 100 };
        Filter = new();
        FilterWorkplaces();

        log.Info("WorkplacesViewModel created");
    }

    [RelayCommand]
    public void FilterWorkplaces(string? reset = null)
    {
        if (reset == "1")
            Filter.PageNumber = 1;
        (IEnumerable<Workplace> workplaces, int count) = workplaceService.GetWorkplacesByFilter(Filter);
        Workplaces = new(workplaces, Filter.PageNumber, Filter.PageSize, count);
    }

    [RelayCommand]
    void ViewWorkplace()
    {
        if (SelectedWorkplace != null)
        {
            WorkplaceView workplaceView = new() { DataContext = factory.CreateViewModel(SelectedWorkplace) }; 
            workplaceView.Show();
            SelectedWorkplace = null;
        }
    }

    [RelayCommand]
    void CreateWorkplace()
    {
        //NewWorkplaceView view = new();
        //view.Show();
    }

    [RelayCommand]
    void FirstPage()
    {
        if (Filter.PageNumber > 1)
        {
            Filter.PageNumber = 1;
            FilterWorkplaces();
        }
    }        

    [RelayCommand]
    void PreviousPage()
    {
        if (Filter.PageNumber > 1)
        {
            Filter.PageNumber--;
            FilterWorkplaces();
        }
    }

    [RelayCommand]
    void NextPage()
    {
        if (Filter.PageNumber < Workplaces?.TotalPages)
        {
            Filter.PageNumber++;
            FilterWorkplaces();
        }
    }

    [RelayCommand]
    void LastPage()
    {
        if (Filter.PageNumber < Workplaces?.TotalPages)
        {
            Filter.PageNumber = Workplaces.TotalPages;
            FilterWorkplaces();
        }
    }

    [RelayCommand]
    static void Close(Window window)
    {
        window.Visibility = Visibility.Collapsed;
    }
}
