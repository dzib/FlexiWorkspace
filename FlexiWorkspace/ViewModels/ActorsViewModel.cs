﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Common.Entities;
using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Views;
using log4net;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;

namespace FlexiWorkspace.ViewModels;

public partial class ActorsViewModel : ObservableObject
{
    static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
    readonly IFactory<ActorViewModel> factory;
    readonly IActorService actorService;

    [ObservableProperty] Actor? _selectedActor;
    [ObservableProperty] PagedResult<Actor>? _actors;
    [ObservableProperty] ActorFilter _filter;
    [ObservableProperty] IEnumerable<int> _resultsNumber;

    public ActorsViewModel(IFactory<ActorViewModel> factory, IActorService actorService)
    {
        this.factory = factory;
        this.actorService = actorService;

        ResultsNumber = new List<int>() { 10, 25, 50, 100 };
        Filter = new();
        FilterActors();

        log.Info("ActorsViewModel created");
    }

    [RelayCommand]
    public void FilterActors(string? reset = null)
    {
        if (reset == "1")
            Filter.PageNumber = 1;
        (IEnumerable<Actor> actors, int count) = actorService.GetActorsByFilter(Filter);
        Actors = new(actors, Filter.PageNumber, Filter.PageSize, count);
    }

    [RelayCommand]
    void ViewActor()
    {
        if (SelectedActor != null)
        {
            ActorView actorView = new() { DataContext = factory.CreateViewModel(SelectedActor) };
            actorView.Show();
            SelectedActor = null;
        }
    }

    [RelayCommand]
    void CreateActor()
    {
        ActorView actorView = new() { DataContext = factory.CreateViewModel(new Actor(), true) };
        actorView.Show();
    }

    [RelayCommand]
    void FirstPage()
    {
        if (Filter.PageNumber > 1)
        {
            Filter.PageNumber = 1;
            FilterActors();
        }
    }        

    [RelayCommand]
    void PreviousPage()
    {
        if (Filter.PageNumber > 1)
        {
            Filter.PageNumber--;
            FilterActors();
        }
    }

    [RelayCommand]
    void NextPage()
    {
        if (Filter.PageNumber < Actors?.TotalPages)
        {
            Filter.PageNumber++;
            FilterActors();
        }
    }

    [RelayCommand]
    void LastPage()
    {
        if (Filter.PageNumber < Actors?.TotalPages)
        {
            Filter.PageNumber = Actors.TotalPages;
            FilterActors();
        }
    }

    [RelayCommand]
    static void Close(Window window)
    {
        window.Visibility = Visibility.Collapsed;
    }
}
