﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace FlexiWorkspace;

public class Factory<T>(IServiceProvider serviceProvider) : IFactory<T>
{
    public T CreateViewModel(object parameter, bool isNew = false)
    {
        return (T)ActivatorUtilities.CreateInstance(serviceProvider, typeof(T), parameter, isNew);
    }
}

