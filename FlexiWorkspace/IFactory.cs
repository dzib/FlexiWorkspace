﻿namespace FlexiWorkspace;

public interface IFactory<T>
{
    T CreateViewModel(object parameter, bool isNew = false);
}

