﻿using FlexiWorkspace.ViewModels;
using Microsoft.Extensions.DependencyInjection;

namespace FlexiWorkspace;

public class ViewModelLocator
{
    public static DashboardViewModel? DashboardViewModel
        => App.AppHost?.Services.GetRequiredService<DashboardViewModel>();

    public static ActorsViewModel? ActorsViewModel
        => App.AppHost?.Services.GetRequiredService<ActorsViewModel>();

    public static ActorViewModel? ActorViewModel
        => App.AppHost?.Services.GetRequiredService<ActorViewModel>();

    public static ContractsViewModel? ContractsViewModel
        => App.AppHost?.Services.GetRequiredService<ContractsViewModel>();

    public static ContractViewModel? ContractViewModel
        => App.AppHost?.Services.GetRequiredService<ContractViewModel>();

    public static WorkplacesViewModel? WorkplacesViewModel
        => App.AppHost?.Services.GetRequiredService<WorkplacesViewModel>();

    public static WorkplaceViewModel? WorkplaceViewModel
        => App.AppHost?.Services.GetRequiredService<WorkplaceViewModel>();
}
