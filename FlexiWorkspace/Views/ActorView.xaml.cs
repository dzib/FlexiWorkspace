﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlexiWorkspace.Views;

public partial class ActorView : Window
{
    public ActorView()
    {
        InitializeComponent();
    }

    void TextBoxMouseEnter(object sender, MouseEventArgs e)
    {
        TextBox textBox = (TextBox)sender;
        textBox.Cursor = Cursors.Hand;
    }

    void TextBoxMouseLeave(object sender, MouseEventArgs e)
    {
        TextBox textBox = (TextBox)sender;
        textBox.Cursor = Cursors.Arrow;
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
        base.OnMouseLeftButtonDown(e);
        try
        {
            DragMove();
        }
        catch (InvalidOperationException) { }
    }

    void Minimize(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState.Minimized;
    }

    void Maximize(object sender, RoutedEventArgs e)
    {
        if (WindowState == WindowState.Maximized)
            WindowState = WindowState.Normal;
        else WindowState = WindowState.Maximized;
    }    
}
