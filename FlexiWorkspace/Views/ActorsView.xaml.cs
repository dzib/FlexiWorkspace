﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlexiWorkspace.Views;

public partial class ActorsView : Window
{
    public ActorsView()
    {
        InitializeComponent();
        CheckBoxesInit();
        if (Left == 0 && Top == 0)
        {
            Left = (SystemParameters.PrimaryScreenWidth - Width) / 2;
            Top = (SystemParameters.PrimaryScreenHeight - Height) / 2;
        }
    }

    void CheckBoxesInit()
    {
        chkName.IsChecked = true;
        chkAddress.IsChecked = true;
        chkMobile.IsChecked = true;
        chkEmail.IsChecked = true;
    }

    void CheckBoxClick(object sender, RoutedEventArgs e)
    {
        Dictionary<string, int> columns = new()
        {
            { "chkId", 0 },
            { "chkName", 1 },
            { "chkAddress", 2 },
            { "chkVat", 3 },
            { "chkMobile", 4 },
            { "chkPhone", 5 },
            { "chkEmail", 6 },
            { "chkContractNum", 7 },
            { "chkWorkplaceNum", 8 }
        };

        CheckBox checkBox = (CheckBox)sender;

        if (checkBox.IsChecked == true)
            dgActors.Columns[columns[checkBox.Name]].Visibility = Visibility.Visible;
        else
            dgActors.Columns[columns[checkBox.Name]].Visibility = Visibility.Collapsed;
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
        base.OnMouseLeftButtonDown(e);
        try
        {
            DragMove();
        }
        catch (InvalidOperationException) { }
    }

    void Minimize(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState.Minimized;
    }

    void Maximize(object sender, RoutedEventArgs e)
    {
        if (WindowState == WindowState.Maximized)
            WindowState = WindowState.Normal;
        else WindowState = WindowState.Maximized;
    }
}
