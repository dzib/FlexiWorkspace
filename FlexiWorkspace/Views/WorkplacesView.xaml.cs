﻿using FlexiWorkspace.Data.Entities;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlexiWorkspace.Views;

public partial class WorkplacesView : Window
{
    public WorkplacesView()
    {
        InitializeComponent();
        CheckBoxesInit();
        if (Left == 0 && Top == 0)
        {
            Left = (SystemParameters.PrimaryScreenWidth - Width) / 2;
            Top = (SystemParameters.PrimaryScreenHeight - Height) / 2;
        }
    }

    void CheckBoxesInit()
    {
        chkAddress.IsChecked = true;
        chkRent.IsChecked = true;
        chkSurface.IsChecked = true;
    }

    void CheckBoxClick(object sender, RoutedEventArgs e)
    {
        Dictionary<string, int> columns = new()
        {
            { "chkId", 0 },
            { "chkAddress", 1 },
            { "chkRent", 2 },
            { "chkSurface", 3 },
            { "chkRenter", 4 },
        };

        CheckBox checkBox = (CheckBox)sender;

        if (checkBox.IsChecked == true)
            dgWorkplaces.Columns[columns[checkBox.Name]].Visibility = Visibility.Visible;
        else
            dgWorkplaces.Columns[columns[checkBox.Name]].Visibility = Visibility.Collapsed;
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
        base.OnMouseLeftButtonDown(e);
        try
        {
            DragMove();
        }
        catch (InvalidOperationException) { }
    }

    void Minimize(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState.Minimized;
    }

    void Maximize(object sender, RoutedEventArgs e)
    {
        if (WindowState == WindowState.Maximized)
            WindowState = WindowState.Normal;
        else WindowState = WindowState.Maximized;
    }
}
