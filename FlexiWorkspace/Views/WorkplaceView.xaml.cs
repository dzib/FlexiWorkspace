﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlexiWorkspace.Views;

public partial class WorkplaceView : Window
{
    public WorkplaceView()
    {
        InitializeComponent();       
    }

    void EnterKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
            Keyboard.ClearFocus();
    }

    void TextBoxMouseEnter(object sender, MouseEventArgs e)
    {
        TextBox textBox = (TextBox)sender;
        textBox.Cursor = Cursors.Hand;
    }

    void TextBoxMouseLeave(object sender, MouseEventArgs e)
    {
        TextBox textBox = (TextBox)sender;
        textBox.Cursor = Cursors.Arrow;
    }

    void RenterEdit(object sender, RoutedEventArgs e)
    {
        renter.IsEnabled = true;
        renter.Style = (Style)FindResource(typeof(ComboBox));        
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
        base.OnMouseLeftButtonDown(e);
        try
        {
            DragMove();
        }
        catch (InvalidOperationException) { }
    }

    void Minimize(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState.Minimized;
    }

    void Maximize(object sender, RoutedEventArgs e)
    {
        if (WindowState == WindowState.Maximized)
            WindowState = WindowState.Normal;
        else WindowState = WindowState.Maximized;
    }
}
