﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlexiWorkspace.Views;

public partial class ContractsView : Window
{
    public ContractsView()
    {
        InitializeComponent();
        CheckBoxesInit();
        if (Left == 0 && Top == 0)
        {
            Top = (SystemParameters.PrimaryScreenHeight - Height) / 2;
            Left = (SystemParameters.PrimaryScreenWidth - Width) / 2;
        }
    }

    void CheckBoxesInit()
    {
        chkCreationDate.IsChecked = true;
        chkStartDate.IsChecked = true;
        chkEndDate.IsChecked = true;
        chkAddress.IsChecked = true;
        chkRent.IsChecked = true;
    }

    void CheckBoxClick(object sender, RoutedEventArgs e)
    {
        Dictionary<string, int> columns = new()
        {
            { "chkId", 0 },
            { "chkCreationDate", 1 },
            { "chkAddress", 2 },
            { "chkStartDate", 3 },
            { "chkEndDate", 4 },
            { "chkRent", 5 },
            { "chkRenter", 6 },
            { "chkTenant", 7 }
        };

        CheckBox checkBox = (CheckBox)sender;

        if (checkBox.IsChecked == true)
            dgContracts.Columns[columns[checkBox.Name]].Visibility = Visibility.Visible;
        else
            dgContracts.Columns[columns[checkBox.Name]].Visibility = Visibility.Collapsed;
    }

    protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
    {
        base.OnMouseLeftButtonDown(e);
        try
        {
            DragMove();
        }
        catch (InvalidOperationException) { }
    }

    void Minimize(object sender, RoutedEventArgs e)
    {
        WindowState = WindowState.Minimized;
    }

    void Maximize(object sender, RoutedEventArgs e)
    {
        if (WindowState == WindowState.Maximized)
            WindowState = WindowState.Normal;
        else WindowState = WindowState.Maximized;
    }
}
