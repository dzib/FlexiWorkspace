﻿using FlexiWorkspace.Business.Services;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Data.Repositories;
using FlexiWorkspace.Data.Repositories.Interfaces;
using FlexiWorkspace.Themes;
using FlexiWorkspace.ViewModels;
using FlexiWorkspace.Views;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Configuration;
using System.Windows;
using System.Windows.Markup;

namespace FlexiWorkspace;

public partial class App : Application
{
    public static IHost? AppHost { get; set; }

    public App()
    {
        AppHost = Host.CreateDefaultBuilder().ConfigureServices((hostContext, services) =>
        {
            // DbContext
            services.AddDbContext<FlexiDbContext>(options => options
                .UseSqlServer(ConfigurationManager.ConnectionStrings["Db"].ConnectionString)
                .UseLazyLoadingProxies(true));

            // Factories
            services.AddTransient(typeof(IFactory<ActorViewModel>),typeof(Factory<ActorViewModel>));
            services.AddTransient(typeof(IFactory<ContractViewModel>), typeof(Factory<ContractViewModel>));
            services.AddTransient(typeof(IFactory<WorkplaceViewModel>), typeof(Factory<WorkplaceViewModel>));

            // Views
            services.AddSingleton(typeof(DashboardView)); 
            services.AddSingleton(typeof(ActorsView));
            services.AddSingleton(typeof(ContractsView));
            services.AddSingleton(typeof(WorkplacesView));

            // ViewModels
            services.AddSingleton(typeof(DashboardViewModel));
            services.AddSingleton(typeof(ActorsViewModel));
            services.AddTransient(typeof(ActorViewModel));
            services.AddSingleton(typeof(ContractsViewModel));
            services.AddTransient(typeof(ContractViewModel));
            services.AddSingleton(typeof(WorkplacesViewModel));
            services.AddTransient(typeof(WorkplaceViewModel));

            // Services
            services.AddTransient(typeof(IDashboardService), typeof(DashboardService));
            services.AddTransient(typeof(IActorService), typeof(ActorService));
            services.AddTransient(typeof(IContractService), typeof(ContractService));
            services.AddTransient(typeof(IWorkplaceService), typeof(WorkplaceService));

            // Repositories
            services.AddTransient(typeof(IGenericRepository), typeof(GenericRepository));

            // Entities
            services.AddTransient(typeof(Actor));
            services.AddTransient(typeof(Contract));
            services.AddTransient(typeof(Workplace));
        }).Build();

        FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), 
            new FrameworkPropertyMetadata(XmlLanguage.GetLanguage("fr-BE")));
    }    

    protected override async void OnStartup(StartupEventArgs e)
    {
        await AppHost!.StartAsync();
        ThemeManager.LoadTheme(Settings.Default.Theme);

        DashboardView startupView = AppHost.Services.GetRequiredService<DashboardView>();
        startupView.Show();

        base.OnStartup(e);
    }

    protected override async void OnExit(ExitEventArgs e)
    {
        await AppHost!.StopAsync();
        base.OnExit(e);
    }
}
