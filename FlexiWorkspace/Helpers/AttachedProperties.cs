﻿using System.Collections.Generic;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace FlexiWorkspace.Helpers;

/// <summary>
/// Définit une propriété attachée pour un bouton permettant de définir une image en tant que chaîne de caractères.
/// </summary>
public static class ButtonProperties
{
    public static readonly DependencyProperty ImageProperty =
            DependencyProperty.RegisterAttached("Image", typeof(string),
            typeof(ButtonProperties), new PropertyMetadata(null));

    public static string GetImage(DependencyObject obj)
    {
        return (string)obj.GetValue(ImageProperty);
    }

    public static void SetImage(DependencyObject obj, string value)
    {
        obj.SetValue(ImageProperty, value);
    }
}