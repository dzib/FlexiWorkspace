﻿namespace FlexiWorkspace.Helpers;

public static class Constants
{
    public const string Version = "1.0.5";
    public const string NumRegex = @"^\d+$";
    public const string EmailRegex = @"/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/";
}
