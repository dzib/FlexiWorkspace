﻿using System.Windows.Data;

namespace FlexiWorkspace.Helpers;

/// <summary>
/// Cette classe représente une extension de liaison utilisée pour se connecter aux paramètres d'application.
/// </summary>
public class SettingsExtension : Binding
{
    public SettingsExtension()
    {
        Initialize();
    }

    public SettingsExtension(string path) : base(path)
    {
        Initialize();
    }

    private void Initialize()
    {
        Source = Settings.Default;
        Mode = BindingMode.TwoWay;
    }
}
