﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace FlexiWorkspace.Helpers;

public static class StringExtensions
{
    /// <summary>
    /// Met la première lettre de la chaîne en majuscule.
    /// Lance une exception si la chaîne est null ou vide.
    /// </summary>
    public static string Capitalize(this string input) =>
    input switch
    {
        null => throw new ArgumentNullException(nameof(input)),
        "" => throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input)),
        _ => string.Concat(input[0].ToString().ToUpper(), input.AsSpan(1))
    };

    /// <summary>
    /// Récupère la description d'une valeur d'énumération à partir de son attribut DescriptionAttribute, 
    /// ou la génère si elle est absente.
    /// </summary>
    public static string Description(this Enum value)
    {
        var attributes = value.GetType().GetField(value.ToString())?
            .GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attributes!.Any())
            return (attributes!.First() as DescriptionAttribute)!.Description;

        TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
        return ti.ToTitleCase(ti.ToLower(value.ToString().Replace("_", " ")));
    }
}
