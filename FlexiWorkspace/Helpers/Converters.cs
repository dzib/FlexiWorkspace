﻿using FlexiWorkspace.Common.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace FlexiWorkspace.Helpers;

/// <summary>
/// Convertisseur de valeurs permettant de convertir un type enum en une collection de valeurs et de descriptions.
/// </summary>
[ValueConversion(typeof(Enum), typeof(IEnumerable<ValueDescription>))]
public class EnumToCollectionConverter : MarkupExtension, IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return GetAllValuesAndDescriptions(value.GetType());
    }
    public object? ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
        return this;
    }

    public static IEnumerable<ValueDescription> GetAllValuesAndDescriptions(Type t)
    {
        if (!t.IsEnum)
            throw new ArgumentException($"{nameof(t)} must be an enum type");

        return Enum.GetValues(t).Cast<Enum>()
            .Where(e => e.ToString() != "Undefined")
            .Select((e) => new ValueDescription() { Value = e, Description = e.Description() }).ToList();
    }
}

public class EnumToStringConverter : MarkupExtension, IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return GetDescription((Enum)value);
    }   

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
        return this;
    }

    public static string GetDescription(Enum en)
    {
        Type type = en.GetType();

        MemberInfo[] memInfo = type.GetMember(en.ToString());
        if (memInfo != null && memInfo.Length > 0)
        {
            object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs != null && attrs.Length > 0)
                return ((DescriptionAttribute)attrs[0]).Description;
        }
        return en.ToString();
    }
}

/// <summary>
/// Convertisseur de valeurs permettant de convertir une valeur enum en string avec une description personnalisée.
/// </summary>
public class UpperCaseDateConverter : MarkupExtension, IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        culture = new CultureInfo("fr-BE");
        return $"{((DateTime)value).ToString((string)parameter, culture)[..1].ToUpperInvariant()}{((DateTime)value).ToString((string)parameter, culture)[1..]}";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
        return this;
    }
}

/// <summary>
/// Cette classe représente un convertisseur de binding utilisé 
/// pour convertir plusieurs bindings en un objet de type CommandParameters.
/// </summary>
class MultiBindingConverter : MarkupExtension, IMultiValueConverter
{
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
        CommandParameters commandParameters = new();
        commandParameters.Property1 = values[0];
        commandParameters.Property2 = values[1];
        commandParameters.Property3 = values[2];
        commandParameters.Property4 = values[3];
        commandParameters.Property5 = values[4];
        return commandParameters;
    }
    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
        return this;
    }
}

public class CommandParameters
{
    public object? Property1 { get; set; }
    public object? Property2 { get; set; }
    public object? Property3 { get; set; }
    public object? Property4 { get; set; }
    public object? Property5 { get; set; }
}