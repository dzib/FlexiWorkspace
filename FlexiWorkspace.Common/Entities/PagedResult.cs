﻿namespace FlexiWorkspace.Common.Entities;

public class BasePagedResult<T>
{
    public int PageNumber { get; set; }
    public int PageSize { get; protected set; }
    public IList<T>? Items { get; protected set; }

    public BasePagedResult(IEnumerable<T> source, int pageNumber, int pageSize)
    {
        PageSize = pageSize;
        PageNumber = pageNumber;
        Items = source?.ToList();
    }
}

public class PagedResult<T> : BasePagedResult<T>
{
    public int TotalCount { get; private set; }
    public int TotalPages => TotalCount > 0 && PageSize > 0 ? (int)Math.Ceiling(TotalCount / (double)PageSize) : 0;
    public int FirstItem => (PageNumber - 1) * PageSize + 1;
    public int LastItem => PageNumber * PageSize < TotalCount ? PageNumber * PageSize : TotalCount;

    public PagedResult(IEnumerable<T> source, int pageNumber, int pageSize, int totalCount) : base(source, pageNumber, pageSize)
    {
        TotalCount = totalCount;
    }
}
