﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.ComponentModel;
using static FlexiWorkspace.Common.Input.ContractFilter;
using static FlexiWorkspace.Data.Entities.Contract;

namespace FlexiWorkspace.Common.Input;

public partial class ActorFilter : ObservableObject
{
    [ObservableProperty] string? _name;
    [ObservableProperty] string? _vat;
    [ObservableProperty] string? _contact;    
    [ObservableProperty] string? _address;
    [ObservableProperty] int _pageNumber;
    [ObservableProperty] int _pageSize;
    [ObservableProperty] EActorSortBy _sortBy;
    [ObservableProperty] ESortDirection _sortDirection;

    public ActorFilter(int pageNumber = 1, int pageSize = 10)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
        SortBy = EActorSortBy.Name;
        SortDirection = ESortDirection.Ascending;
    }

    public enum EActorSortBy
    {
        [Description("Identifiant")]
        Id = 0,
        [Description("Nom")]
        Name = 1,
        [Description("Adresse")]
        Address = 2,
        [Description("Nombre de locaux")]
        WorkplaceNumber = 3,
        [Description("Nombre de contrats")]
        ContractNumber = 4
    }
}

public partial class ContractFilter : ObservableObject
{
    [ObservableProperty] EStatus _status;
    [ObservableProperty] string? _renter;
    [ObservableProperty] string? _tenant;
    [ObservableProperty] string? _address;
    [ObservableProperty] int? _rentMin;
    [ObservableProperty] int? _rentMax;
    [ObservableProperty] int _pageNumber;
    [ObservableProperty] int _pageSize;
    [ObservableProperty] EContractSortBy _sortBy;
    [ObservableProperty] ESortDirection _sortDirection;

    public ContractFilter(int pageNumber = 1, int pageSize = 10)
    {
        Status = EStatus.All;
        PageNumber = pageNumber;
        PageSize = pageSize;
        SortBy = EContractSortBy.CreationDate;
        SortDirection = ESortDirection.Ascending;
    }

    public enum EContractSortBy
    {
        [Description("Identifiant")]
        Id = 0,
        [Description("Date de signature")]
        CreationDate = 1,
        [Description("Adresse")]
        Address = 2,
        [Description("Date de début")]
        StartDate = 3,
        [Description("Date de fin")]
        EndDate = 4,
        [Description("Loyer")]
        Rent = 5,
        [Description("Propriétaire")]
        Renter = 6,
        [Description("Locataire")]
        Tenant = 7
    }    
}

public partial class WorkplaceFilter : ObservableObject
{
    [ObservableProperty] string? _renter;
    [ObservableProperty] string? _address;
    [ObservableProperty] int? _rentMin;
    [ObservableProperty] int? _rentMax;
    [ObservableProperty] int? _surfaceMin;
    [ObservableProperty] int? _surfaceMax;
    [ObservableProperty] int _pageNumber;
    [ObservableProperty] int _pageSize;
    [ObservableProperty] EWorkplaceSortBy _sortBy;
    [ObservableProperty] ESortDirection _sortDirection;

    public WorkplaceFilter(int pageNumber = 1, int pageSize = 10)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
        SortBy = EWorkplaceSortBy.Address;
        SortDirection = ESortDirection.Ascending;
    }

    public enum EWorkplaceSortBy
    {
        [Description("Identifiant")]
        Id = 0,
        [Description("Adresse")]
        Address = 1,
        [Description("Propriétaire")]
        Renter = 2
    }
}

public enum ESortDirection
{
    [Description("Ascendant")]
    Ascending = 0,
    [Description("Descendant")]
    Descending = 1,
}
