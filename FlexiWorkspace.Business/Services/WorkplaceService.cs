﻿using FlexiWorkspace.Business.Helpers;
using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Services;

public class WorkplaceService : IWorkplaceService
{
    private readonly IGenericRepository repository;

    public WorkplaceService(IGenericRepository repository)
    {
        this.repository = repository;
    }

    public IEnumerable<Workplace> GetAllWorkplaces()
    {
        return repository.Get<Workplace>(w => w.Active);
    }

    public IEnumerable<Workplace> GetWorkplaces(Expression<Func<Workplace, bool>>? expression)
    {
        return repository.Get(expression);
    }

    public (IEnumerable<Workplace>, int) GetWorkplacesByFilter(WorkplaceFilter filter)
    {
        Expression<Func<Workplace, bool>> expression = _ => true;

        if (filter.Renter?.Length > 2)
            expression = expression.AndAlso(c =>
                c.Renter!.OrganisationName != null ?
                c.Renter!.OrganisationName.ToLower().Contains(filter.Renter.ToLower()) :
                c.Renter!.LastName!.ToLower().Contains(filter.Renter.ToLower()) ||
                c.Renter!.FirstName!.ToLower().Contains(filter.Renter.ToLower()));

        if (filter.RentMin.HasValue)
            expression = expression.AndAlso(c => c.Rent >= filter.RentMin);

        if (filter.RentMax.HasValue)
            expression = expression.AndAlso(c => c.Rent <= filter.RentMax);

        if (filter.SurfaceMin.HasValue)
            expression = expression.AndAlso(c => c.Surface >= filter.SurfaceMin);

        if (filter.SurfaceMax.HasValue)
            expression = expression.AndAlso(c => c.Surface <= filter.SurfaceMax);

        string? orderBy = null;
        switch (filter.SortBy)
        {
            case WorkplaceFilter.EWorkplaceSortBy.Id:
                orderBy = "WorkplaceId";
                break;
            case WorkplaceFilter.EWorkplaceSortBy.Address:
                orderBy = "Contact.Street";
                break;
            case WorkplaceFilter.EWorkplaceSortBy.Renter:
                orderBy = "w => w.Renter.OrganisationName == null ? Renter.LastName : Renter.OrganisationName";
                break;
        }

        if (filter.SortDirection == ESortDirection.Descending)
            orderBy += " desc";

        IEnumerable<Workplace> workplaces = repository.Get(expression, orderBy, null, filter.PageNumber, filter.PageSize);
        int count = repository.GetCount(expression);

        return (workplaces, count);
    }

    public Workplace? GetWorkplace(int workplaceId)
    {
        return repository.GetById<Workplace>(workplaceId);
    }

    public void AddWorkplace(Workplace workplace)
    {
        repository.Add(workplace);
        repository.Save();
    }

    public void UpdateWorkplace(Workplace workplace)
    {
        repository.Update(workplace);
        repository.Save();
    }

    public void DeactivateWorkplace(Workplace workplace)
    {
        workplace.Active = false;
        UpdateWorkplace(workplace);
    }

    public IEnumerable<Feature> GetAllFeatures()
    {
        return repository.GetAll<Feature>();
    }

    public void AddWorkplaceFeature(WorkplaceFeature workplaceFeature)
    {
        repository.Add(workplaceFeature);
    }

    public void RemoveWorkplaceFeature(WorkplaceFeature workplaceFeature)
    {
        repository.Delete(workplaceFeature);
    }
}
