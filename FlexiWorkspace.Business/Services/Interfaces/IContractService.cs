﻿using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Services.Interfaces;

public interface IContractService
{
    IEnumerable<Contract> GetAllContracts();
    IEnumerable<Contract> GetContracts(Expression<Func<Contract, bool>>? expression);
    (IEnumerable<Contract>, int) GetContractsByFilter(ContractFilter filter);
    Contract? GetContract(int contractId);
    void AddContract(Contract contract);
    void UpdateContract(Contract contract);
    void CancelContract(Contract contract);
    bool CheckOverlappingDates(Contract contract);
}