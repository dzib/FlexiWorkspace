﻿using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Services.Interfaces
{
    public interface IWorkplaceService
    {
        IEnumerable<Workplace> GetAllWorkplaces();
        IEnumerable<Workplace> GetWorkplaces(Expression<Func<Workplace, bool>>? expression);
        (IEnumerable<Workplace>, int) GetWorkplacesByFilter(WorkplaceFilter filter);
        Workplace? GetWorkplace(int workplaceId);
        void AddWorkplace(Workplace workplace);
        void UpdateWorkplace(Workplace workplace);
        void DeactivateWorkplace(Workplace workplace);
        IEnumerable<Feature> GetAllFeatures();
        void AddWorkplaceFeature(WorkplaceFeature workplaceFeature);
        void RemoveWorkplaceFeature(WorkplaceFeature workplaceFeature);
    }
}