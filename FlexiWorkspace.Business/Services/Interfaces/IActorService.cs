﻿using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Services.Interfaces
{
    public interface IActorService
    {
        IEnumerable<Actor> GetAllActors();
        IEnumerable<Actor> GetActors(Expression<Func<Actor, bool>>? expression);
        (IEnumerable<Actor>, int) GetActorsByFilter(ActorFilter filter);
        Actor? GetActor(int actorId);
        void AddActor(Actor actor);
        void UpdateActor(Actor actor);
        void DeactivateActor(Actor actor);
    }
}