﻿using FlexiWorkspace.Data.Entities;
using System;
using System.Collections.Generic;

namespace FlexiWorkspace.Business.Services.Interfaces;

public interface IDashboardService
{
    int GetActorsCount();
    int GetOrganisationsCount();
    int GetPersonsCount();
    int GetActorsCountByMonth(int year, int month);
    IEnumerable<Actor> GetLastActors(int count);
    int GetRunningContractsCount();
    decimal? GetSumRents();
    TimeSpan GetAverageLength();
    int GetContractsCountByMonth(int year, int month);
    IEnumerable<Contract> GetEndingContracts(int count);
    int GetWorkplacesCount();
    int GetWorkplacesAvailableCount();
    int GetOccupancyRate();
    int GetWorkplacesCountByMonth(int year, int month);
}