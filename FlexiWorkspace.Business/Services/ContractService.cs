﻿using FlexiWorkspace.Business.Helpers;
using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Services;

public class ContractService : IContractService
{
    private readonly IGenericRepository repository;

    public ContractService(IGenericRepository repository)
    {
        this.repository = repository;
    }

    public IEnumerable<Contract> GetAllContracts()
    {
        return repository.Get<Contract>(w => !w.Cancelled);
    }

    public IEnumerable<Contract> GetContracts(Expression<Func<Contract, bool>>? expression)
    {
        return repository.Get(expression);
    }

    public (IEnumerable<Contract>, int) GetContractsByFilter(ContractFilter filter)
    {
        Expression<Func<Contract, bool>> expression = _ => true;

        if (filter.Status != Contract.EStatus.All)
        {
            if (filter.Status == Contract.EStatus.Planned)
                expression = expression.AndAlso(c => c.StartDate > DateTime.Now);
            if (filter.Status == Contract.EStatus.Running)
                expression = expression.AndAlso(c => c.StartDate < DateTime.Now && c.EndDate > DateTime.Now);
            if (filter.Status == Contract.EStatus.Archived)
                expression = expression.AndAlso(c => c.EndDate < DateTime.Now);
            if (filter.Status == Contract.EStatus.Cancelled)
                expression = expression.AndAlso(c => c.Cancelled);
        }

        if (filter.Renter?.Length > 2)
            expression = expression.AndAlso(c =>
                c.Workplace!.Renter!.OrganisationName != null ?
                c.Workplace!.Renter!.OrganisationName.ToLower().Contains(filter.Renter.ToLower()) :
                c.Workplace!.Renter!.LastName!.ToLower().Contains(filter.Renter.ToLower()) ||
                c.Workplace!.Renter!.FirstName!.ToLower().Contains(filter.Renter.ToLower()));

        if (filter.Tenant?.Length > 2)
            expression = expression.AndAlso(c =>
                c.Tenant!.OrganisationName != null ?
                c.Tenant!.OrganisationName.ToLower().Contains(filter.Tenant.ToLower()) :
                c.Tenant!.LastName!.ToLower().Contains(filter.Tenant.ToLower()) ||
                c.Tenant!.FirstName!.ToLower().Contains(filter.Tenant.ToLower()));

        if (filter.Address?.Length > 2)
            expression = expression.AndAlso(c =>
                c.Workplace!.Contact!.Street!.ToLower().Contains(filter.Address.ToLower()) ||
                c.Workplace!.Contact!.Number!.ToLower().Contains(filter.Address.ToLower()) ||
                c.Workplace!.Contact!.Zip.ToString()!.ToLower().Contains(filter.Address.ToLower()) ||
                c.Workplace!.Contact!.City!.ToLower().Contains(filter.Address.ToLower()));

        if (filter.RentMin.HasValue)
            expression = expression.AndAlso(c => c.Rent >= filter.RentMin);

        if (filter.RentMax.HasValue)
            expression = expression.AndAlso(c => c.Rent <= filter.RentMax);

        string? orderBy = null;
        switch (filter.SortBy)
        {
            case ContractFilter.EContractSortBy.Id:
                orderBy = "ContractId";
                break;
            case ContractFilter.EContractSortBy.CreationDate:
                orderBy = "CreationDate";
                break;
            case ContractFilter.EContractSortBy.Address:
                orderBy = "Workplace.Contact.Street";
                break;
            case ContractFilter.EContractSortBy.StartDate:
                orderBy = "StartDate";
                break;
            case ContractFilter.EContractSortBy.EndDate:
                orderBy = "EndDate";
                break;
            case ContractFilter.EContractSortBy.Rent:
                orderBy = "Rent";
                break;
            case ContractFilter.EContractSortBy.Renter:
                orderBy = "c => c.Workplace.Renter.OrganisationName == null ? Workplace.Renter.LastName : Workplace.Renter.OrganisationName";
                break;
            case ContractFilter.EContractSortBy.Tenant:
                orderBy = "c => c.Tenant.OrganisationName == null ? Tenant.LastName : Tenant.OrganisationName";
                break;
        }

        if (filter.SortDirection == ESortDirection.Descending)
            orderBy += " desc";

        IEnumerable<Contract> contracts = repository.Get(expression, orderBy, null, filter.PageNumber, filter.PageSize);
        int count = repository.GetCount(expression);

        return (contracts, count);
    }

    public Contract? GetContract(int contractId)
    {
        return repository.GetById<Contract>(contractId);
    }

    public void AddContract(Contract contract)
    {
        repository.Add(contract);
        repository.Save();
    }

    public void UpdateContract(Contract contract)
    {
        repository.Update(contract);
        repository.Save();
    }

    public void CancelContract(Contract contract)
    {
        contract.Cancelled = true;
        contract.CancelledDate = DateTime.Now; 
        UpdateContract(contract);
    }

    public bool CheckOverlappingDates(Contract newContract)
    {
        var contracts = repository.GetAll<Contract>();

        foreach (var existingContract in contracts)
        {
            bool isOverlapping =
                (existingContract.StartDate <= newContract.EndDate && existingContract.EndDate >= newContract.StartDate) |
                (existingContract.StartDate >= newContract.EndDate && existingContract.EndDate <= newContract.StartDate);

            if (isOverlapping && !existingContract.Cancelled && existingContract.WorkplaceId == newContract.WorkplaceId)
                return true;
        }

        return false;
    }

}
