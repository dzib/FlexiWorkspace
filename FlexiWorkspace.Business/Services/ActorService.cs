﻿using FlexiWorkspace.Business.Helpers;
using FlexiWorkspace.Common.Input;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Services;

public class ActorService : IActorService
{
    private readonly IGenericRepository repository;

    public ActorService(IGenericRepository repository)
    {
        this.repository = repository;
    }

    public IEnumerable<Actor> GetAllActors()
    {
        return repository.Get<Actor>(w => w.Active);
    }

    public IEnumerable<Actor> GetActors(Expression<Func<Actor, bool>>? expression)
    {
        return repository.Get(expression);
    }

    public (IEnumerable<Actor>, int) GetActorsByFilter(ActorFilter filter)
    {
        Expression<Func<Actor, bool>> expression = _ => true;

        if (filter.Name?.Length > 2)
            expression = expression.AndAlso(a =>
                a.OrganisationName != null ?
                a.OrganisationName.ToLower().Contains(filter.Name.ToLower()) :
                a.LastName!.ToLower().Contains(filter.Name.ToLower()) ||
                a.FirstName!.ToLower().Contains(filter.Name.ToLower()));

        if (filter.Vat?.Length > 2)
            expression = expression.AndAlso(a => a.Vat!.ToLower().Contains(filter.Vat.ToLower()));

        if (filter.Contact?.Length > 2)
            expression = expression.AndAlso(a =>
                (a.Contact!.Phone != null && a.Contact.Phone.ToLower().Contains(filter.Contact.ToLower())) ||
                (a.Mobile != null && a.Mobile.ToLower().Contains(filter.Contact.ToLower())) ||
                (a.Email != null && a.Email.ToLower().Contains(filter.Contact.ToLower())));

        if (filter.Address?.Length > 2)
            expression = expression.AndAlso(a =>
                a.Contact!.Street!.ToLower().Contains(filter.Address.ToLower()) ||
                a.Contact!.Number!.ToLower().Contains(filter.Address.ToLower()) ||
                a.Contact!.Zip.ToString()!.ToLower().Contains(filter.Address.ToLower()) ||
                a.Contact!.City!.ToLower().Contains(filter.Address.ToLower()));

        string? orderBy = null;
        switch (filter.SortBy)
        {
            case ActorFilter.EActorSortBy.Id:
                orderBy = "ActorId";
                break;
            case ActorFilter.EActorSortBy.Name:
                orderBy = "a => a.OrganisationName == null ? LastName : OrganisationName";
                break;
            case ActorFilter.EActorSortBy.Address:
                orderBy = "Contact.Street";
                break;
            case ActorFilter.EActorSortBy.ContractNumber:
                orderBy = "Contracts.Count";
                break;
            case ActorFilter.EActorSortBy.WorkplaceNumber:
                orderBy = "Workplaces.Count";
                break;
        }

        if (filter.SortDirection == ESortDirection.Descending)
            orderBy += " desc";

        IEnumerable<Actor> actors = repository.Get(expression, orderBy, null, filter.PageNumber, filter.PageSize);
        int count = repository.GetCount(expression);

        return (actors, count);
    }

    public Actor? GetActor(int actorId)
    {
        return repository.GetById<Actor>(actorId);
    }

    public void AddActor(Actor actor)
    {
        repository.Add(actor);
        repository.Save();
    }

    public void UpdateActor(Actor actor)
    {        
        repository.Update(actor);
        repository.Save();
    }

    public void DeactivateActor(Actor actor)
    {
        actor.Active = false;
        UpdateActor(actor);
    }
}
