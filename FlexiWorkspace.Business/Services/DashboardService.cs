﻿using FlexiWorkspace.Business.Helpers;
using FlexiWorkspace.Business.Services.Interfaces;
using FlexiWorkspace.Data.Entities;
using FlexiWorkspace.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlexiWorkspace.Business.Services;

public partial class DashboardService : IDashboardService
{
    private readonly IGenericRepository repository;

    public DashboardService(IGenericRepository repository)
    {
        this.repository = repository;
    }

    #region Actors
    public int GetActorsCount()
    {
        return repository.GetCount<Actor>(a => a.Active);
    }

    public int GetOrganisationsCount()
    {
        return repository.GetCount<Actor>(a => a.Active && a.OrganisationName != null);
    }

    public int GetPersonsCount()
    {
        return repository.GetCount<Actor>(a => a.Active && a.OrganisationName == null);
    }

    public int GetActorsCountByMonth(int year, int month)
    {
        return repository.GetCount<Actor>(c => c.CreationDate.Year == year && c.CreationDate.Month == month);
    }

    public IEnumerable<Actor> GetLastActors(int count)
    {
        return repository.Get<Actor>(c => c.Active, "CreationDate desc").Take(count);
    }
    #endregion

    #region Contracts
    public int GetRunningContractsCount()
    {
        return repository.GetCount<Contract>(c => c.StartDate < DateTime.Now && c.EndDate > DateTime.Now);
    }

    public decimal? GetSumRents()
    {
        return repository.Sum<Contract>(c => c.Rent, c => c.StartDate < DateTime.Now && c.EndDate > DateTime.Now);
    }

    public TimeSpan GetAverageLength()
    {
        IEnumerable<Contract> contracts = repository.GetAll<Contract>();
        return contracts.Sum(c => c.EndDate - c.StartDate) / contracts.Count();
    }

    public int GetContractsCountByMonth(int year, int month)
    {
        return repository.GetCount<Contract>(c => c.CreationDate.Year == year && c.CreationDate.Month == month);
    }

    public IEnumerable<Contract> GetEndingContracts(int count)
    {
        return repository.Get<Contract>(c => !c.Cancelled && c.EndDate > DateTime.Now, "EndDate").Take(count);
    }
    #endregion              

    #region Workplaces
    public int GetWorkplacesCount()
    {
        return repository.GetCount<Workplace>(a => a.Active);
    }

    public int GetWorkplacesAvailableCount()
    {
        return repository.GetCount<Workplace>(w => w.Active && !w.Contracts.Any(c => DateTime.Now > c.StartDate && DateTime.Now < c.EndDate));
    }   

    public int GetOccupancyRate()
    {
        int workplacesRented = repository.GetCount<Workplace>(w => w.Active && w.Contracts.Any(c => DateTime.Now > c.StartDate && DateTime.Now < c.EndDate));
        int workplaces = repository.GetCount<Workplace>(w => w.Active);

        return (int)Math.Round((double)(100 * workplacesRented) / workplaces);
    }

    public int GetWorkplacesCountByMonth(int year, int month)
    {
        return repository.GetCount<Workplace>(c => c.CreationDate.Year == year && c.CreationDate.Month == month);
    }
    #endregion    
}
