﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FlexiWorkspace.Business.Helpers;

public static class LinqExtensions
{
    private class ParameterRebinder : ExpressionVisitor
    {
        private readonly Dictionary<ParameterExpression, ParameterExpression> _map;

        private ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
        {
            _map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
        }

        public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
        {
            return new ParameterRebinder(map).Visit(exp);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            if (_map.TryGetValue(p, out var value))
            {
                p = value;
            }

            return base.VisitParameter(p);
        }
    }

    public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
    {
        return first.Compose(second, new Func<Expression, Expression, Expression>(Expression.AndAlso));
    }

    private static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
    {
        Expression<T> second2 = second;
        Dictionary<ParameterExpression, ParameterExpression> map = first.Parameters.Select((ParameterExpression f, int i) => new
        {
            f = f,
            s = second2.Parameters[i]
        }).ToDictionary(p => p.s, p => p.f);
        Expression arg = ParameterRebinder.ReplaceParameters(map, second2.Body);
        return Expression.Lambda<T>(merge(first.Body, arg), first.Parameters);
    }
}
