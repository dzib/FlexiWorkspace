﻿using CommunityToolkit.Mvvm.ComponentModel;
using FlexiWorkspace.Data.Entities;
using System;
using System.Collections.ObjectModel;

namespace FlexiWorkspace.Business.Entities;

/// <summary>
/// Entité représentant le tableau de bord qui sera affiché
/// sur la page principale de l'application.
/// </summary>
public partial class Dashboard : ObservableObject
{
    public int Actors { get; set; }
    public int Organisations { get; set; }
    public int Persons { get; set; }
    public int Workplaces { get; set; }
    public int WorkplacesAvailable { get; set; }
    public int OccupancyRate { get; set; }
    public int RunningContracts { get; set; }
    public decimal? SumRents { get; set; }
    public TimeSpan AverageDuration { get; set; }
}
